const noteDetail = (state = { detail: {} }, action) => {
    if (action.type === "GET_NOTE_BY_ID") {
        return {
            ...state,
            detail: action.detail
        }
    } else {
        return state;
    }
};

export default noteDetail;
