import React, {Component} from 'react';
import {connect} from 'react-redux';
import getNoteById from "../actions/getNoteById";

    function mapStateToProps({noteDetail}) {
        return {
            detail: noteDetail.detail
        };
    }

    function mapDispatchToProps(dispatch) {
        return {
            getDetail: (id) => dispatch(getNoteById(id))
        }
    }


    class NoteDetail extends Component {


        constructor(props, context) {
            super(props, context);
        }

        componentDidMount() {
            this.props.getDetail(this.props.match.params.id);
        }

        render() {

            return (
                <div>
                    <h1>{this.props.detail.title}</h1>
                    <p>{this.props.detail.description}</p>
                </div>
            );
        }
    }

    export default connect(
        mapStateToProps,mapDispatchToProps
    )(NoteDetail);
