import React, {Component} from 'react';
import {BrowserRouter as Router} from "react-router-dom";
import {Route, Switch} from "react-router";
import './App.less';
import Home from "./home/components/Home";
import NoteDetail from "./note/components/NoteDetail";

class App extends Component{

    render() {
    return (
        <div className="App">
            <Router>
                <Switch>
                    <Route exact path="/" component={Home} />
                    <Route path="/note/:id" component={NoteDetail} />
                </Switch>
            </Router>
        </div>
    );
  }
}

export default App;
