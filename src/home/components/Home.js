import React, {Component} from 'react';
import {connect} from 'react-redux';

import getNoteList from "../actions/getNoteList";
import {Link} from "react-router-dom";

function mapStateToProps(state) {
    return {
        notes: state.notes.notes
    };
}

function mapDispatchToProps(dispatch) {
    return {
        getNotes: () => dispatch(getNoteList())
    }
}

class Home extends Component {
    constructor(props, context) {
        super(props, context);
    }

    componentDidMount() {
        this.props.getNotes();
    }

    render() {
        return (
            <div>
                <header>
                    Note
                </header>
                <body>
                <ul>
                    {this.props.notes.map(item => {
                        let uri = '/note/' + item.id;
                        return (<li><Link to={uri}>{item.title}</Link></li>)
                    })}
                </ul>
                </body>
            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
