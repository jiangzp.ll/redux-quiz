import React, {Component} from 'react';
import {connect} from 'react-redux';

function mapStateToProps(state) {
    return {state};
}

class NoteDetail extends Component {
    render() {
        return (
            <div>
                <h1>title</h1>
                <p>description</p>

            </div>
        );
    }
}

export default connect(
    mapStateToProps,
)(NoteDetail);
